#!/bin/sh

set -e

if [ -z "${1}" ]
then
  echo "Usage: ${0} <lambda-function-name>" >&2
  exit 1
fi

if [ ! -d build ]
then
  echo 'Build directory not found' >&2
  exit 1
fi

cp index.js package.json build
cd build
yarn install --production

rm -f lambda.zip
zip -r lambda.zip node_modules/ package.json index.js

export AWS_PAGER=""
aws lambda update-function-code --zip-file fileb://./lambda.zip --function-name "${1}"
rm -rf node_modules index.js package.json yarn.lock

echo 'Function code updated, cleanup complete. Press ENTER to publish, CTRL+C to cancel'
# shellcheck disable=SC2034
read -r UNUSED
aws lambda publish-version --function-name "${1}"
