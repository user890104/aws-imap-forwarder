import { ImapFlow } from 'imapflow';
import { GetObjectCommand, S3Client } from '@aws-sdk/client-s3';

export async function handler(event) {
    [
        'S3_REGION',
        'S3_BUCKET',

        'IMAP_HOSTNAME',
        'IMAP_PORT',
        'IMAP_SECURE',
        'IMAP_USERNAME',
        'IMAP_PASSWORD',
    ].forEach(function (envKey) {
        if (!process.env.hasOwnProperty(envKey)) {
            throw new Error('Missing required environment variable ' + envKey);
        }
    });

    const records = event?.Records || [];

    if (records.length === 0) {
        return;
    }

    const s3Client = new S3Client({
        region: process.env.S3_REGION,
    });

    // Initialize IMAP client
    const client = new ImapFlow({
        host: process.env.IMAP_HOSTNAME,
        port: parseInt(process.env.IMAP_PORT, 10),
        secure: ['true', 'yes', '1'].includes(process.env.IMAP_SECURE),
        auth: {
            user: process.env.IMAP_USERNAME,
            pass: process.env.IMAP_PASSWORD,
        },
    });

    // Wait until client connects and authorizes
    await client.connect();

    for (const record of records) {
        // Verify event record
        if (record.eventSource !== 'aws:ses' || record.eventVersion !== '1.0' || !record.hasOwnProperty('ses')) {
            console.error('Invalid record', record.eventSource, record.eventVersion);
            continue;
        }

        // Extract messageId
        const messageId = record.ses?.mail?.messageId;

        if (messageId.length === 0) {
            console.error('Invalid messageId', messageId);
        }

        console.log('Processing messageId', messageId);

        // Calculate source object's key
        const keyPrefix = process.env.S3_KEY_PREFIX;
        const key = (typeof keyPrefix === 'string' && keyPrefix.length > 0 ? (keyPrefix + '/') : '') + messageId;

        // Retrieve object from S3
        const messageObject = await s3Client.send(
            new GetObjectCommand({
                Bucket: process.env.S3_BUCKET,
                Key: key,
            }),
        );

        // Convert Body stream to Buffer
        const messageBuffer = Buffer.from(await messageObject.Body.transformToByteArray());

        // Store the message in IMAP
        await client.append(process.env.IMAP_MAILBOX, messageBuffer);
    }

    // log out and close connection
    await client.logout();
}
